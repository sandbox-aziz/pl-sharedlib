
def call(Map params = null, Closure body = null){
    node(){
        try{
            if(!body){
                build(params)
            } else{
                body()
            }
        } catch(err){
            def errorMessage = err.getMessage()
            echo 'Error:' + errorMessage
        }finally {
            cleanWs()

        }
    }
}
void build(Map params = null){
}