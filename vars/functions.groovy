void setParams(){
    sh"sed -i 's/ENV/${params.environment}/g' $WORKSPACE/vars/terraform.tfvars"
    sh"sed -i 's/TEAM/${params.team}/g' $WORKSPACE/vars/terraform.tfvars"
    sh"sed -i 's/REGION/${params.region}/g' $WORKSPACE/vars/terraform.tfvars"
    sh"sed -i 's#CIDR#${params.cidr}#g' $WORKSPACE/vars/terraform.tfvars"
    sh"sed -i 's#PUBLIC#${params.public_cidr}#g' $WORKSPACE/vars/terraform.tfvars"
    sh"sed -i 's#PRIVATE#${params.private_cidr}#g' $WORKSPACE/vars/terraform.tfvars"
    sh"sed -i 's#AZS#${params.azs}#g' $WORKSPACE/vars/terraform.tfvars"
    sh"sed -i 's/BUCKET/${params.bucket}/g' $WORKSPACE/vars/terraform.tfvars"
    sh"sed -i 's/REGION/${params.region}/g' $WORKSPACE/provider.tf"
    sh"cat $WORKSPACE/vars/terraform.tfvars"
}


void terraformInit(){
    def tfworkspace = "${params.environment}-${params.team}"
    sh"""
    terraform init -no-color
    terraform workspace select -no-color ${tfworkspace} || terraform workspace new -no-color ${tfworkspace}
    terraform workspace show -no-color
    terraform validate -no-color
    """
}

void terraformPlan(){
    sh"terraform plan -no-color -var-file $WORKSPACE/vars/terraform.tfvars"
}

void terraformApply(){
    sh"terraform apply  -var-file $WORKSPACE/vars/terraform.tfvars -no-color -auto-approve"
}

void terraformDestroy(){
    sh"terraform destroy -var-file $WORKSPACE/vars/terraform.tfvars -no-color -auto-approve"
}